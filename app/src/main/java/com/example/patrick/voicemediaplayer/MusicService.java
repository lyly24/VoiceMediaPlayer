package com.example.patrick.voicemediaplayer;

        import android.app.Notification;
        import android.app.PendingIntent;
        import android.app.Service;
        import android.content.ContentUris;
        import android.content.Intent;
        import android.media.AudioManager;
        import android.media.MediaPlayer;
        import android.net.Uri;
        import android.os.Binder;
        import android.os.IBinder;
        import android.os.PowerManager;
        import android.provider.MediaStore;
        import android.util.Log;

        import java.io.IOException;
        import java.util.ArrayList;


/**
 * Created by Patrick on 22.05.2018.
 */

public class  MusicService extends Service implements
        MediaPlayer.OnErrorListener,
        MediaPlayer.OnCompletionListener {

    private float volume =0.5f;

    private final IBinder musicBind = new MusicBinder();
    //media player
    private MediaPlayer player;
    //media player for voice
    private MediaPlayer voice;
    //song list
    public ArrayList<Song> songs;
    //current position
    public int songPosn;

    private String songTitle="";
    private static final int NOTIFY_ID=1;

    private DataManager dataManager;

    public Song playSong;

    public void initMusicPlayer(){
        //set player properties
        player.setWakeMode(getApplicationContext(),
                PowerManager.PARTIAL_WAKE_LOCK);
        player.setAudioStreamType(AudioManager.STREAM_MUSIC);
        player.setOnCompletionListener(new MediaPlayer.OnCompletionListener(){

            @Override
            public void onCompletion(MediaPlayer mp) {
                if(player.getCurrentPosition()>=0){
                    playNext();
                }
            }
        });
        player.setOnErrorListener(this);
    }

    public void onCreate(){
        //create the service
        //create the service
        super.onCreate();
//initialize position
        songPosn=0;
//create player
        player = new MediaPlayer();
        player.setVolume(0.5f, 0.5f);
        initMusicPlayer();
        dataManager=new DataManager(getContentResolver());
        songs=dataManager.getSongList();
    }

    public void filterPlaylist(String constraint) {
        ArrayList<Song> tempList = new ArrayList<Song>();

        // search content in friend list
        for (Song song : songs) {
            if (song.getArtist().toLowerCase().contains(constraint.toString().toLowerCase())) {
                tempList.add(song);
            }
            songs = tempList;
        }
    }


//make listener that listens when new song is played


    public void setList(ArrayList<Song> theSongs){
        songs=theSongs;
    }

    public void playSong(){
        //play a song
        player.reset();
        //get song
        playSong = songs.get(songPosn);
        //set the songtitle
        songTitle=playSong.getTitle();
        //get id
        long currSong = playSong.getID();
        //set uri
        Uri trackUri = ContentUris.withAppendedId(
                android.provider.MediaStore.Audio.Media.EXTERNAL_CONTENT_URI,
                currSong);
        try{
            player.setDataSource(getApplicationContext(), trackUri);
        }
        catch(Exception e){
            Log.e("MUSIC SERVICE", "Error setting data source", e);
        }
        player.prepareAsync();
    }

    public void playNext(){
        songPosn=(songPosn+1)%(songs.size()-1);
        playSong();
    }

    public void playPrev(){
        if((songPosn-1)>=0){
            songPosn--;
        }else {
            songPosn=songs.size()-1;
        }
        playSong();
    }

    public MediaPlayer getPlayer(){
        return player;
    }

    public int getPosn(){
        return player.getCurrentPosition();
    }

    public int getDur(){
        return player.getDuration();
    }

    public boolean isPng(){
        return player.isPlaying();
    }

    public void pausePlayer(){
        player.pause();
    }

    public void seek(int posn){
        player.seekTo(posn);
    }

    public void go(){
        player.start();
    }

    public void playSong(String str){
        int pos=0;
        for(Song song:songs){
            String tempString =song.getTitle().toLowerCase();
            if (tempString.equals(str)){
                songPosn=pos;
                playSong();
                return;
            }
            pos++;
        }
    }


    public void setVolume(float volume){
        //     int maxVolume=((AudioManager)getSystemService(AUDIO_SERVICE)).getStreamMaxVolume(AudioManager.STREAM_MUSIC); //hier 15
        //    float log1=(float)(Math.log(maxVolume-currVolume)/Math.log(maxVolume));
        //  float volume=1-log1;
        player.setVolume(volume,volume);
        this.volume=volume;
    }


    public void setVolumeWithoutSaving(float volume){
        //     int maxVolume=((AudioManager)getSystemService(AUDIO_SERVICE)).getStreamMaxVolume(AudioManager.STREAM_MUSIC); //hier 15
        //    float log1=(float)(Math.log(maxVolume-currVolume)/Math.log(maxVolume));
        //  float volume=1-log1;
        player.setVolume(volume,volume);
    }

    public void mute(){
        ((AudioManager) getSystemService(AUDIO_SERVICE)).setStreamVolume(AudioManager.STREAM_MUSIC,0, 0);
    }

    public void increaseVolume(float x) throws IOException {
        //   int maxVolume=((AudioManager)getSystemService(AUDIO_SERVICE)).getStreamMaxVolume(AudioManager.STREAM_MUSIC); //hier 15
        //   int currVolume=((AudioManager) getSystemService(AUDIO_SERVICE)).getStreamVolume(AudioManager.STREAM_MUSIC);
        float  newvolume=volume+x;
        //   int volume=currVolume+x;
        //float log1=(float)(Math.log(maxVolume-currVolume)/Math.log(maxVolume));
        //float volume=1-log1;
        //  ((AudioManager) getSystemService(AUDIO_SERVICE)).setStreamVolume(AudioManager.STREAM_MUSIC, volume, 0);
        player.setVolume(newvolume,newvolume);
        volume=newvolume;

    }

    @Override
    public IBinder onBind(Intent intent) {
        return musicBind;
    }

    @Override
    public boolean onUnbind(Intent intent){

        return false;
    }

    @Override
    public void onCompletion(MediaPlayer mp) {
        if(voice.getCurrentPosition()>=0) {
            player.setVolume(volume, volume);
        }
        if(voice!=null){
            voice.release();
            //voice=null;
        }
    }

    public void setToOldVolume(){

            player.setVolume(volume, volume);
    }

    @Override
    public boolean onError(MediaPlayer mp, int what, int extra) {
        return false;
    }


    public void onPreparedAction() {
        //start playback
        player.start();
        Intent notIntent = new Intent(this, MainActivity.class);
        notIntent.addFlags( Intent.FLAG_ACTIVITY_CLEAR_TOP |Intent.FLAG_ACTIVITY_SINGLE_TOP);
        notIntent.addFlags( Intent.FLAG_ACTIVITY_CLEAR_TOP |Intent.FLAG_ACTIVITY_SINGLE_TOP);

        PendingIntent pendInt = PendingIntent.getActivity(this, 0,
                notIntent, 0);

        Notification.Builder builder = new Notification.Builder(this);

        builder.setContentIntent(pendInt)
                .setSmallIcon(R.drawable.play)
                .setTicker(songTitle)
                .setOngoing(true)
                .setContentTitle("Playing")
                .setContentText(songTitle);
        Notification not = builder.build();
        not.flags = Notification.FLAG_ONGOING_EVENT | Notification.FLAG_NO_CLEAR;
        startForeground(NOTIFY_ID, not);

    }



    public void setSong(int songIndex){
        songPosn=songIndex;
    }


    public class MusicBinder extends Binder {
        MusicService getService() {
            return MusicService.this;
        }
    }
    //make the volume lower so the user understands pls

    public void greeting() {
        voice = MediaPlayer.create(this, R.raw.halloschoendassdudabist);
        // player.
        // voice.setVolume();
        voice.setOnCompletionListener(this);
        voice.start();
    }

    public void didntUnderstand(){
        if(volume>0.2f) {
            player.setVolume(0.2f, 0.2f);
        }
        voice = MediaPlayer.create(this, R.raw.dashabeichleidernichtverstanden);
        voice.setOnCompletionListener(this);
        voice.start();
    }

    public void nuscheln(){
        if(volume>0.2f) {
            player.setVolume(0.2f, 0.2f);
        }
        voice = MediaPlayer.create(this, R.raw.bittenichtsonuschelnfunny);
        voice.setOnCompletionListener(this);
        voice.start();
    }

    public float getVolume(){
        return volume;
    }

    @Override
    public void onDestroy() {

        stopForeground(true);
        player.stop();
        player.release();
    }


}
