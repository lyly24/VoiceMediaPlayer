package com.example.patrick.voicemediaplayer;

import android.content.ContentUris;
import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.SeekBar;
import android.widget.TextView;

import java.util.ArrayList;


public class MainFragment extends Fragment {

    private ImageView viewAlbum;
    private TextView viewSongName;
    private TextView viewArtist;
    private SeekBar mSeekbar;
    private TextView mMediaTime;
    private int current = 0;
    private int duration = 0;


    public MainFragment() {
        // Required empty public constructor
    }

    public static MainFragment newInstance(int param1, String param2) {
        MainFragment fragment = new MainFragment();

        return fragment;
    }

    public void setRunning(boolean run){
      //  running=run;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view= inflater.inflate(R.layout.fragment_main, container, false);
        viewSongName=view.findViewById(R.id.songName);
        viewArtist= view.findViewById(R.id.songArtist);
        mSeekbar= view.findViewById(R.id.seekBar);
        mMediaTime= view.findViewById(R.id.mMediaTime);
        viewAlbum= view.findViewById(R.id.songAlbum);

        return view;
    }


    public void updateSong(String songName, String artist,long path, int duration ){
        viewSongName.setText(songName);
        viewArtist.setText(artist);
        Uri sArtworkUri = Uri.parse("content://media/external/audio/albumart");
        Uri albumArtUri = ContentUris.withAppendedId(sArtworkUri,path);
        viewAlbum.setImageURI(null);
        viewAlbum.setImageURI(albumArtUri);
        mSeekbar.setMax(duration);
        mSeekbar.postDelayed(onEverySecond, 1000);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }




    private Runnable onEverySecond = new Runnable() {
        @Override
        public void run() {

            if (mSeekbar != null) {
                mSeekbar.postDelayed(onEverySecond, 1000);
                mSeekbar.setProgress(((MainActivity) getActivity()).getCurrentPosition());
            }

            if (mSeekbar != null) {
                //mSeekbar.postDelayed(onEverySecond, 1000);
                //updateTime();
            }
        }
    };

/**
        private void updateTime(){
            do {
                current = (((MainActivity)getActivity()).getCurrentPosition());
                int dSeconds = (int) (duration / 1000) % 60 ;
                int dMinutes = (int) ((duration / (1000*60)) % 60);
                int dHours   = (int) ((duration / (1000*60*60)) % 24);

                int cSeconds = (int) (current / 1000) % 60 ;
                int cMinutes = (int) ((current / (1000*60)) % 60);
                int cHours   = (int) ((current / (1000*60*60)) % 24);

                if(dHours == 0){
                    mMediaTime.setText(String.format("%02d:%02d / %02d:%02d", cMinutes, cSeconds, dMinutes, dSeconds));
                }else{
                    mMediaTime.setText(String.format("%02d:%02d:%02d / %02d:%02d:%02d", cHours, cMinutes, cSeconds, dHours, dMinutes, dSeconds));
                }
                try{
                    if(mSeekbar.getProgress() >= 100){
                        break;
                    }
                }catch (Exception e) {}

            }while (mSeekbar.getProgress() <= 100);
        }



**/

}
