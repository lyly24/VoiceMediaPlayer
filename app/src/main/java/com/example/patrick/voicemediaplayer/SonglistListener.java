package com.example.patrick.voicemediaplayer;

import android.view.View;

/**
 * Created by Patrick on 24.05.2018.
 */

public interface SonglistListener {

     void onSongClicked(View view, int position);
}
