package com.example.patrick.voicemediaplayer;

/**
 * Created by Patrick on 22.05.2018.
 */

public class Song {
    private long id;
    private String title;
    private String artist;
    private long albumId;


    public Song(long songID, String songTitle, String songArtist, long songAlbum) {
        id=songID;
        title=songTitle;
        artist=songArtist;
        albumId=songAlbum;
    }


    public long getID(){return id;}
    public String getTitle(){return title;}
    public String getArtist(){return artist;}
    public long getPathAlbumCover() {
        return albumId;
    }
}
