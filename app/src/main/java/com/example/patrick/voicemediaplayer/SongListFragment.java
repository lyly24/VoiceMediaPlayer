package com.example.patrick.voicemediaplayer;

import android.content.Context;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;


import java.util.ArrayList;


public class SongListFragment extends Fragment{
    private RecyclerView songView;
    private SongAdapter songAdt;
    private RecyclerView.LayoutManager layoutManager;

    public SongListFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.

     * @return A new instance of fragment SongListFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static SongListFragment newInstance(int page, String title) {
        SongListFragment songListFragment = new SongListFragment();
        return songListFragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view= inflater.inflate(R.layout.fragment_song_list, container, false);
        songView = (RecyclerView)view.findViewById(R.id.song_list);
        songView.setLayoutManager(layoutManager);
        songView.setAdapter(songAdt);

        songAdt.setOnSongClickedListener(new SonglistListener(){
            @Override
            public void onSongClicked(View view, int position){
                ((MainActivity)getActivity()).setSongList(songAdt.getFilteredList());
                ((MainActivity)getActivity()).songPicked(position);
            }
        });
        return view;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        ArrayList<Song> songList=((MainActivity)getActivity()).getSongList();
        songAdt = new SongAdapter( songList);
        layoutManager = new LinearLayoutManager(context);
            }

    ArrayList<Song> getFilteredList(){
        return songAdt.getFilteredList();
    }

    public void filter(String str){
        songAdt.getFilter().filter(str);
        ((MainActivity)getActivity()).setSongList(songAdt.getFilteredList());
            }

}
