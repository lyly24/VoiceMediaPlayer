package com.example.patrick.voicemediaplayer;

import android.content.ContentUris;
import android.content.Context;
import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by Patrick on 22.05.2018.
 */

public class SongAdapter extends RecyclerView.Adapter<SongAdapter.ViewHolder> implements Filterable{
    private ArrayList<Song> songs;
    private SonglistListener mListener;
    private ArrayList<Song> filteredList;
    private SongFilter songFilter;

    public ArrayList<Song> getFilteredList(){
        return filteredList;
    }


    public SongAdapter( ArrayList<Song> theSongs){
        songs=theSongs;
        filteredList=theSongs;
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{
        public TextView songView;
        public TextView artistView;
        public ImageView albumCover;
        //public ImageView imageView;

        ViewHolder(View v ) {
            super(v);
            //get title and artist views
             songView = (TextView)v.findViewById(R.id.song_title);
             artistView = (TextView)v.findViewById(R.id.song_artist);
             albumCover = (ImageView)v.findViewById(R.id.album);
            //get title and artist strings
            v.setOnClickListener(this);
            getFilter();
        }

        @Override
        public void onClick(View view) {
            mListener.onSongClicked(view, getAdapterPosition());
        }
    }


    @Override
    public SongAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        // create a new view
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View v = inflater.inflate(R.layout.song, parent, false);
        // set the view's size, margins, paddings and layout parameters
        ViewHolder vh = new ViewHolder(v);
        return vh;

    }

    @Override
    public void onBindViewHolder(SongAdapter.ViewHolder holder, final int position) {

        // - get element from your dataset at this position
        // - replace the contents of the view with that element
        holder.songView.setText( filteredList.get(position).getTitle());
        holder.artistView.setText( filteredList.get(position).getArtist());
        Uri sArtworkUri = Uri.parse("content://media/external/audio/albumart");
        Uri albumArtUri = ContentUris.withAppendedId(sArtworkUri,filteredList.get(position).getPathAlbumCover() );
        holder.albumCover.setImageURI(null);
        holder.albumCover.setImageURI(albumArtUri);
    }

    public void setOnSongClickedListener(SonglistListener listener){
        mListener=listener;
    }



    @Override
    public int getItemCount() {
        return filteredList.size();
    }


    @Override
    public Filter getFilter() {
        if (songFilter == null) {
            songFilter = new SongFilter();
        }
        return songFilter;
    }

    private class SongFilter extends Filter {

        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            FilterResults filterResults = new FilterResults();
            if (constraint!=null && constraint.length()>0) {
                ArrayList<Song> tempList = new ArrayList<Song>();

                // search content in friend list
                for (Song song : songs) {
                    if (song.getArtist().toLowerCase().contains(constraint.toString().toLowerCase())) {
                        tempList.add(song);
                    }
                }
                filterResults.count = tempList.size();
                filterResults.values = tempList;
            } else {
                filterResults.count = songs.size();
                filterResults.values = songs;
            }
            return filterResults;
        }

        /**
         * Notify about filtered list to ui
         * @param constraint text
         * @param results filtered result
         */
        @SuppressWarnings("unchecked")
        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            filteredList = (ArrayList<Song>) results.values;
            notifyDataSetChanged();
        }
    }

}