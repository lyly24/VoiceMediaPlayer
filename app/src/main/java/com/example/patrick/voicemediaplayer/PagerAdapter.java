package com.example.patrick.voicemediaplayer;

/**
 * Created by Patrick on 22.05.2018.
 */

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

/**
 * Created by Patrick on 18.05.2018.
 */
public class PagerAdapter extends FragmentPagerAdapter {
    private static int NUM_ITEMS = 2; //3 Ansichten

    public PagerAdapter(FragmentManager fragmentManager) {
        super(fragmentManager);
    }

    // Returns total number of pages
    @Override
    public int getCount() {
        return NUM_ITEMS;
    }

    // Returns the fragment to display for that page
    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0: // Fragment # 0 - This will show FirstFragment, (mit Hilfe einer statischen Methode)
                return MainFragment.newInstance(0,"Hauptmenu");

            case 1:
                return SongListFragment.newInstance(1, "Songs");

            default:
                return null;
        }
    }



}
