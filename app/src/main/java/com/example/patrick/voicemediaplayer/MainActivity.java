package com.example.patrick.voicemediaplayer;

import android.Manifest;
import android.app.SearchManager;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.pm.PackageManager;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Build;
import android.os.IBinder;
import android.provider.Settings;
import android.speech.RecognitionListener;
import android.speech.RecognizerIntent;
import android.speech.SpeechRecognizer;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ImageButton;
import android.widget.MediaController;
import android.widget.TextView;
import android.support.v4.app.FragmentActivity;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;


public class MainActivity extends FragmentActivity implements MediaPlayer.OnPreparedListener{

    FragmentPagerAdapter adapterViewPager;
    private MusicService musicSrv;
    private Intent playIntent;
    private boolean musicBound;
    private TextView viewText;
    private SpeechRecognizer mSpeechRecognizer;
    private Intent mSpeechRecognizerIntent;
    private ImageButton microButton;
    private Map<String,Method> instructionsMap;
    private ViewPager vpPager;
    private boolean paused=false, playbackPaused=false;
    private DataManager dataManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        adapterViewPager = new PagerAdapter(getSupportFragmentManager());
        vpPager = (ViewPager) findViewById(R.id.vpPager);
        vpPager.setAdapter(adapterViewPager);

        checkPermission();
        checkPermissionStorage();


        try {
            setupInstructions();
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        }

        mSpeechRecognizerIntent = new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);
        mSpeechRecognizerIntent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL,
                RecognizerIntent.LANGUAGE_MODEL_FREE_FORM);
        mSpeechRecognizerIntent.putExtra(RecognizerIntent.EXTRA_LANGUAGE,
                Locale.getDefault());

        viewText= findViewById(R.id.textView);
        microButton= findViewById(R.id.button);
        microButton.setOnTouchListener(microOnClickListener);
        mSpeechRecognizer = SpeechRecognizer.createSpeechRecognizer(this);
        dataManager=new DataManager(getContentResolver());

        mSpeechRecognizer.setRecognitionListener(new RecognitionListener() {
            @Override
            public void onReadyForSpeech(Bundle bundle) {

            }

            @Override
            public void onBeginningOfSpeech() {
            if(musicSrv!=null){
                if(musicSrv.getVolume()>0.2f)
                musicSrv.setVolumeWithoutSaving(0.2f);
            }
            }

            @Override
            public void onRmsChanged(float v) {

            }

            @Override
            public void onBufferReceived(byte[] bytes) {

            }

            @Override
            public void onEndOfSpeech() {
                if(musicSrv!=null)
            musicSrv.setToOldVolume();
            }

            @Override
            public void onError(int i) {

            }
            @Override
            public void onResults(Bundle bundle) {
                //getting all the matches
                ArrayList<String> matches = bundle
                        .getStringArrayList(SpeechRecognizer.RESULTS_RECOGNITION);

                //displaying the first match
                if (matches != null)
                    viewText.setText(matches.get(0));
                try {
                    followInstruction(matches.get(0));
                } catch (InvocationTargetException e) {
                    e.printStackTrace();
                } catch (IllegalAccessException e) {
                    e.printStackTrace();
                }
            }
            @Override
            public void onPartialResults(Bundle bundle) {

            }

            @Override
            public void onEvent(int i, Bundle bundle) {

            }
        });

    }


    private View.OnTouchListener microOnClickListener=new View.OnTouchListener() {
        @Override
        public boolean onTouch(View view, MotionEvent motionEvent) {
            switch (motionEvent.getAction()) {
                case MotionEvent.ACTION_UP:
                    mSpeechRecognizer.stopListening();
                    viewText.setHint("You will see input here");
                    break;

                case MotionEvent.ACTION_DOWN:
                    mSpeechRecognizer.startListening(mSpeechRecognizerIntent);
                    viewText.setText("");
                    viewText.setHint("Listening...");
                    break;
            }
            return false;
        }
    };

    public void songPicked(int position){
        musicSrv.setSong(position);
        musicSrv.playSong();
        if(playbackPaused){
            playbackPaused=false;
        }
    }


    private void updateSongUI(){
            MainFragment fragment = (MainFragment) getSupportFragmentManager().findFragmentByTag("android:switcher:" + R.id.vpPager + ":" + 0);
            if(vpPager.getCurrentItem()!=0){vpPager.setCurrentItem(0);}
            adapterViewPager.notifyDataSetChanged();
            int duration=getDuration();
            if(musicSrv.isPng()&&musicBound)
            fragment.updateSong(musicSrv.playSong.getTitle(), musicSrv.playSong.getArtist(), musicSrv.playSong.getPathAlbumCover(), duration);
    }


    private void checkPermission() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (!(ContextCompat.checkSelfPermission(this, Manifest.permission.RECORD_AUDIO) == PackageManager.PERMISSION_GRANTED)) {
                Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS,
                        Uri.parse("package:" + getPackageName()));
                startActivity(intent);
                finish();
            }
        }
    }

    public void checkPermissionStorage(){
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (checkSelfPermission(Manifest.permission.READ_EXTERNAL_STORAGE)
                    != PackageManager.PERMISSION_GRANTED) {

                requestPermissions(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},1);

// MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE is an
// app-defined int constant
                return;
            }
        }
    }


    //connect to the service
    private  ServiceConnection musicConnection = new ServiceConnection(){
        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            MusicService.MusicBinder binder = (MusicService.MusicBinder)service;
            //get service
            musicSrv = binder.getService();
            //pass list
            musicBound = true;
            musicSrv.setList(dataManager.getSongList());
            musicSrv.getPlayer().setOnPreparedListener(MainActivity.this);
            musicSrv.greeting();
        }
        @Override
        public void onServiceDisconnected(ComponentName name) {
            musicBound = false;
        }
    };


    public void setSongList(ArrayList<Song> songs){
        musicSrv.setList(songs);
    }


    @Override
    protected void onStart() {
        super.onStart();
        if(playIntent==null){

            playIntent = new Intent(this, MusicService.class);
            bindService(playIntent, musicConnection, Context.BIND_AUTO_CREATE);
            startService(playIntent);
        }
    }


    public void onSongPicked(View view) {
        musicSrv.setSong(Integer.parseInt(view.getTag().toString()));
        musicSrv.playSong();
    }


    public void setupInstructions() throws NoSuchMethodException {
        instructionsMap = new HashMap<String, Method>();
        instructionsMap.put("nächstes lied", this.getClass().getMethod("nextSong"));
        instructionsMap.put("vorheriges lied", this.getClass().getMethod("previousSong"));
        instructionsMap.put("pause", this.getClass().getMethod("pauseSong"));
        instructionsMap.put("mach mal lauter", this.getClass().getMethod("increaseVolume"));
        instructionsMap.put("sei mal leise", this.getClass().getMethod("mute"));
        instructionsMap.put("random", this.getClass().getMethod("randomSong"));
        instructionsMap.put("stop", this.getClass().getMethod("pauseSong"));
        instructionsMap.put("weiter", this.getClass().getMethod("start"));
        instructionsMap.put("partylautstärke", this.getClass().getMethod("partyLautstaerke"));
        instructionsMap.put("party lautstärke", this.getClass().getMethod("partyLautstaerke"));
        instructionsMap.put("hintergrund", this.getClass().getMethod("leiseLautstaerke"));
        instructionsMap.put("infos", this.getClass().getMethod("search"));
        instructionsMap.put("reset",this.getClass().getMethod("resetSongs"));
    }

    public void followInstruction(String instruction) throws InvocationTargetException, IllegalAccessException {
        instruction=instruction.toLowerCase();

        if(instruction.contains("spiele")) {
            String substr = instruction.substring(7);
            musicSrv.playSong(substr);
        }

        else if(instruction.contains("alle lieder von")) {
            String substr=instruction.substring(16);
            filterSongs(substr);

        }


        else if(instruction.contains("lieder von")) {
            String substr=instruction.substring(11);
            filterSongs(substr);
        }
        else if(instruction.contains("lautstärke")){
            String substr=instruction.substring(11);
            try {
                Integer lautstaerke = Integer.parseInt(substr);
                 float volume = (float)lautstaerke/10;
                 musicSrv.setVolume(volume);
            }
            catch (Exception e){

            }
            }



        else if(instructionsMap.get(instruction)==null) {
                //play random sound *nette weibliche Stimme* "kannst du das bitte wiederholen", "könntest du das nochmal sagen?"
                //sollte asyncrhon sein
           double rand= Math.random();
           if(rand>0.5) {
               didntUnderstand();
           }else{
               nuscheln();
           }
        }else {
            try {
                instructionsMap.get(instruction).invoke(this);
            } catch (InvocationTargetException e) {
                e.printStackTrace();
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            }
        }
    }

    public void filterSongs(String str){
        SongListFragment fragment = (SongListFragment) getSupportFragmentManager().findFragmentByTag("android:switcher:" + R.id.vpPager + ":" + 1);
        adapterViewPager.notifyDataSetChanged();
        fragment.filter(str);
        //if(vpPager.getCurrentItem()!=1)
            vpPager.setCurrentItem(1);
    }

    public void resetSongs(){
        SongListFragment fragment = (SongListFragment) getSupportFragmentManager().findFragmentByTag("android:switcher:" + R.id.vpPager + ":" + 1);
        fragment.filter(null);
        adapterViewPager.notifyDataSetChanged();
    }

    public void leiseLautstaerke(){
        musicSrv.setVolume(0.1f);
    }



    public void nextSong(){
        musicSrv.playNext();
        if(playbackPaused){
            playbackPaused=false;
        }
    }

    public void previousSong(){
        musicSrv.playPrev();
        if(playbackPaused){
            playbackPaused=false;
        }
    }

    public void randomSong(){
        musicSrv.setSong((int) (Math.random()*1000000*musicSrv.songs.size())%musicSrv.songs.size());
        musicSrv.playSong();
    }

    public void seekTo(int pos) {
        musicSrv.seek(pos);
    }


    public void start() {
        musicSrv.go();
    }


    public void pauseSong() {
        playbackPaused=true;
        musicSrv.pausePlayer();
    }

    public int getDuration() {
        if(musicSrv!=null && musicBound &&musicSrv.isPng() )
        return musicSrv.getDur();
  else return 0;
    }

  //Position im Lied
    public int getCurrentPosition() {
        if(musicSrv!=null && musicBound && musicSrv.isPng())
        return musicSrv.getPosn();
  else return 0;
    }


    public MediaPlayer getPlayer(){
       return musicSrv.getPlayer();
    }

    public boolean isPlaying() {
        if(musicSrv!=null && musicBound)
        return musicSrv.isPng();
        return false;
    }


    public void mute(){
        musicSrv.mute();
    }

    @Override
    protected void onPause(){
        super.onPause();
        paused=true;
    }

    @Override
    protected void onResume(){
        super.onResume();
        if(paused){
            paused=false;
        }
    }

    public void partyLautstaerke(){
        musicSrv.setVolume(1.0f);
    }

    ArrayList<Song>  getSongList(){
       return dataManager.getSongList();
    }

    public void increaseVolume() throws IOException {
        musicSrv.increaseVolume(0.1f);
    }

    public void search(){
        if(musicSrv!=null) {
            String query=musicSrv.playSong.getArtist()+" "+musicSrv.playSong.getTitle();
            Intent intent = new Intent(Intent.ACTION_WEB_SEARCH);
            intent.putExtra(SearchManager.QUERY, query);
            if (intent.resolveActivity(getPackageManager()) != null) {
                startActivity(intent);
            }
        }

    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
        stopService(playIntent); //use this only if you want stop the player here
        musicSrv=null; //use this only if you want stop the player here
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    @Override
    protected void onDestroy() {
        if (musicBound) unbindService(musicConnection); // blogger missed this
        musicSrv=null;
        super.onDestroy();
    }

    @Override
    public void onPrepared(MediaPlayer mp) {
        //MainFragment fragment = (MainFragment) getSupportFragmentManager().findFragmentByTag("android:switcher:" + R.id.vpPager + ":" + 0);
        musicSrv.onPreparedAction();
        updateSongUI();
    }

    public void nuscheln(){
        musicSrv.nuscheln();
    }

    public void didntUnderstand(){
        musicSrv.didntUnderstand();
    }
}

